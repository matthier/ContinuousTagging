/*!
   \file binstat.h
   \brief Contains the statistical information of a tag bin
*/

#ifndef BINSTAT_H
#define BINSTAT_H

#include <vector>

using namespace std;

/*! 
\struct binstat 
\brief Contains the statistical information of a tag bin
*/
struct binstat {

  double pdt;  /*!< Tagweight bin data efficiency */
  double pmc;  /*!< Tagweight bin MC efficiency */
  double sf;   /*!< Tagweight bin Scale Factor */
  double v;    /*!< Tagweight bin variance on the number of jets */
  double ntot; /*!< Kinematic bin number of jets (inclusive in tagweight bins) */
  double syst; /*!< Systematic uncertainty on the SF */
  
};

#endif
