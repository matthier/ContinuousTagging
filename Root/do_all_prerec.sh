rm out_charm.root
rm out_light.root
rm out_tau.root
rm out_bottom.root

echo -e '\n'
echo 'Charm'
echo '-----> EM'
export DIR=../Data/DStarTauPreRec # EM jets
echo '--------- MV1'

./continuousTandP $DIR/DStar_MV1c80.txt \
       $DIR/DStar_MV1c70.txt \
       $DIR/DStar_MV1c60.txt \
       $DIR/DStar_MV1c50.txt

echo -e '\n'
echo 'Tau'
echo '-----> EM'
export DIR=../Data/DStarTauPreRec
echo ''
echo '--------- MV1'
./continuousTandP $DIR/DStar_MV1c80_tau.txt \
     $DIR/DStar_MV1c70_tau.txt \
     $DIR/DStar_MV1c60_tau.txt \
     $DIR/DStar_MV1c50_tau.txt 

echo -e '\n'
echo 'Light'
export DIR=../Data/NegativeTagPreRec
./continuousTandP $DIR/mistag_negative_mv1c_jvf5_w0.405_OP80_EM_LHCP2013_v85.txt    \
       $DIR/mistag_negative_mv1c_jvf5_w0.7028_OP70_EM_LHCP2013_v85.txt   \
       $DIR/mistag_negative_mv1c_jvf5_w0.8349_OP60_EM_LHCP2013_v85.txt   \
       $DIR/mistag_negative_mv1c_jvf5_w0.9237_OP50_EM_LHCP2013_v85.txt


echo -e '\n'
echo 'Beauty'
export DIR=../Data/TandP210116 #21
echo '-----> EM'
echo '---------- MV1'

./continuousTandP $DIR/TandP_6bins_emu_2jmva_MV2c20_85WP.txt \
 		  $DIR/TandP_6bins_emu_2jmva_MV2c20_77WP.txt \
 		  $DIR/TandP_6bins_emu_2jmva_MV2c20_70WP.txt \
 		  $DIR/TandP_6bins_emu_2jmva_MV2c20_60WP.txt 


#./continuousTandP $DIR/TandP_6bins_emu_2jmva_MV2c20_85WP.txt \
# 	     $DIR/TandP_6bins_emu_2jmva_MV2c20_50WP.txt \
# 	     $DIR/TandP_6bins_emu_2jmva_MV2c20_30WP.txt
