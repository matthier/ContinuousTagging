#ifndef REDUCTION_H
#define REDUCTION_H

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include <TMatrixD.h>
#include <TH3F.h>

#include "binstat.h"

using namespace std;

// mbin stands for merged bin
// sbin stands for sub bin, i.e. that will be merged in one same bin

bool verbose_reduction = false;

void ApplyReduction(int npTbins, int netabins, const vector<vector<int> > reduction, const vector<vector<binstat> > binStat, vector<vector<binstat> > &r_binStat, const TMatrixD cov, TMatrixD &r_cov, map<string,TH3F*> mth2_sys, map<string,TH3F*> &r_mth2_sys, map<string,TH3F*> mth2_usys, map<string,TH3F*> &r_mth2_usys)
{
  int nkbins = binStat[0].size();
  int r_ntbins = r_binStat.size();
  int ntbins = binStat.size();

  TMatrixD jmat(r_ntbins*nkbins,ntbins*nkbins);

  for(int kbin=0; kbin<nkbins; kbin++){
    int ipt=kbin, ieta=0;
    if (kbin>=npTbins){
      ieta = kbin/npTbins;
      ipt -= ieta*npTbins;
    }
    
    for(int mbin=0; mbin<r_ntbins; mbin++){
      r_binStat[mbin][kbin].pdt  = 0;
      r_binStat[mbin][kbin].pmc  = 0;
      r_binStat[mbin][kbin].sf   = 0;
      r_binStat[mbin][kbin].syst = 0;
      	
      for(int sbin=0; sbin<(int)reduction[mbin].size(); sbin++) {
	int sbin_i = reduction[mbin][sbin];

	r_binStat[mbin][kbin].pdt += binStat[sbin_i][kbin].pdt;
	r_binStat[mbin][kbin].pmc += binStat[sbin_i][kbin].pmc;
	double m_sumeffmc = 0.;
	for(int sbin2=0; sbin2<(int)reduction[mbin].size(); sbin2++){

	  int sbin2_i = reduction[mbin][sbin2];
	  m_sumeffmc += binStat[sbin2_i][kbin].pmc;
	}
	int ind_i = kbin*r_ntbins+mbin;
	int ind_j = kbin*ntbins+sbin_i;
	jmat(ind_i,ind_j) = binStat[sbin_i][kbin].pmc / m_sumeffmc;
	r_binStat[mbin][kbin].sf   += binStat[sbin_i][kbin].sf  *jmat(ind_i,ind_j);
	r_binStat[mbin][kbin].syst += binStat[sbin_i][kbin].syst*jmat(ind_i,ind_j);

	for(map<string,TH3F*>::iterator it = mth2_sys.begin(); it != mth2_sys.end(); it++){
	  r_mth2_sys[it->first]->SetBinContent(ipt+1,ieta+1,mbin+1,
							    r_mth2_sys[it->first]->GetBinContent(ipt+1,ieta+1,mbin+1) + jmat(ind_i,ind_j)*mth2_sys[it->first]->GetBinContent(ipt+1,ieta+1,sbin_i+1));
	}
	for(map<string,TH3F*>::iterator it = mth2_usys.begin(); it != mth2_usys.end(); it++){
	  r_mth2_usys[it->first]->SetBinContent(ipt+1,ieta+1,mbin+1,
							     r_mth2_usys[it->first]->GetBinContent(ipt+1,ieta+1,mbin+1) + jmat(ind_i,ind_j)*mth2_usys[it->first]->GetBinContent(ipt+1,ieta+1,sbin_i+1));
	}	
      } // sub bins

      // reconsider the total systematics
      double tot=0;
      for (map<string,TH3F*>::iterator it = r_mth2_sys.begin(); it != r_mth2_sys.end(); it++){
	double xsyst = it->second->GetBinContent(ipt+1,ieta+1,mbin+1);
	if( (it->first != "systematics") and (it->first != "MCreference") )
	  tot += xsyst*xsyst;
      }
      for (map<string,TH3F*>::iterator it = r_mth2_usys.begin(); it != r_mth2_usys.end(); it++){
	double xsyst = it->second->GetBinContent(ipt+1,ieta+1,mbin+1);
	if( (it->first != "systematics") and (it->first != "MCreference") )
	  tot += xsyst*xsyst;
      }
      r_mth2_sys["systematics"]->SetBinContent(ipt+1,ieta+1,mbin+1,sqrt(tot));
      r_binStat[mbin][kbin].syst = sqrt(tot);
      // change the MCreference value ??
      //r_mth2_sys["MCreference"]->SetBinContent(ipt+1,ieta+1,mbin+1,r_binStat[mbin][kbin].pmc);
      //cout << "MCreference : " << r_binStat[mbin][kbin].pmc << endl;
    } // merged bin
  } // kinematic bin

  double tot=0;
  for (map<string,TH3F*>::iterator it = mth2_sys.begin(); it != mth2_sys.end(); it++){
    double xsyst = it->second->GetBinContent(1,1,1);
    if( (it->first != "systematics") and (it->first != "MCreference"))
      tot += xsyst*xsyst;
  }
  for (map<string,TH3F*>::iterator it = mth2_usys.begin(); it != mth2_usys.end(); it++){
    double xsyst = it->second->GetBinContent(1,1,1);
      tot += xsyst*xsyst;
  }  
  // ----
  
  TMatrixD t_jmat(binStat.size()*nkbins,r_binStat.size()*nkbins);
  t_jmat.Transpose(jmat);
  
  r_cov = jmat*cov*t_jmat;

  if(verbose_reduction){
    cout << "cov"    << endl;   cov   .Print();  
    cout << "r_cov"  << endl;   r_cov .Print();  
    cout << "jmat"   << endl;   jmat  .Print();
    cout << "t_jmat" << endl;   t_jmat.Print();
  }
}
  
#endif
