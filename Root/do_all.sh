rm out_charm.root
#rm out_light.root
rm out_tau.root
#rm out_bottom.root

#echo -e '\n'
#echo 'Charm' # switch to the ttbar calibration for now
source do_charm_ttbar.sh
#export DIR=../Data/CalibrationResults/analyses/2016-20_7-13TeV/cjets/Wc
#
#./continuousTandP $DIR/W+c_85.txt \
#       $DIR/W+c_77.txt \
#       $DIR/W+c_70.txt \
#       $DIR/W+c_60_4bins.txt
#
#echo -e '\n'
#echo 'Tau'
#export DIR=../Data/CalibrationResults/analyses/2016-20_7-13TeV/taujets/Wc
#./continuousTandP $DIR/W+c_85.txt \
#       $DIR/W+c_77.txt \
#       $DIR/W+c_70.txt \
#       $DIR/W+c_60_4bins.txt

#echo -e '\n'
#echo 'Light'
#export DIR=../Data/CalibrationResults/analyses/2016-20_7-13TeV/ljets/negative_tags
#./continuousTandP $DIR/negtag_v00-07_WP85.txt    \
#       $DIR/negtag_v00-07_WP77.txt   \
#       $DIR/negtag_v00-07_WP70.txt   \
#       $DIR/negtag_v00-07_WP60.txt


#echo -e '\n'
#echo 'Beauty'
#export DIR=../Data/CalibrationResults/analyses/2016-20_7-13TeV/bjets/ttbar_topo
#./continuousTandP $DIR/TandP_6bins_emu_2jmva_MV2c20_85WP.txt \
# 		  $DIR/TandP_6bins_emu_2jmva_MV2c20_77WP.txt \
# 		  $DIR/TandP_6bins_emu_2jmva_MV2c20_70WP.txt \
# 		  $DIR/TandP_6bins_emu_2jmva_MV2c20_60WP.txt 


#./continuousTandP $DIR/TandP_6bins_emu_2jmva_MV2c20_85WP.txt \
# 	     $DIR/TandP_6bins_emu_2jmva_MV2c20_50WP.txt \
# 	     $DIR/TandP_6bins_emu_2jmva_MV2c20_30WP.txt
