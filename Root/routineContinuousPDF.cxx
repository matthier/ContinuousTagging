#include <iostream>
#include <TROOT.h>

#include "continuousPDF.C"

void routineContinuousPDF()
{
  gROOT->ProcessLine(".L continuousPDF.C++");

  // following the prototype below
  // void continuousPDF(int nKinBins, int nTagBins, int channelsUsr, int mistagScaleUsr, int useMistagSFUsr)

  
  // -------------------------------------------------
  // All channels
  // for the Mistag scaling 75%
  for(int fitScenario=0; fitScenario<5; fitScenario++)
    continuousPDF(6, 5, 2, 75, fitScenario);
  
  // for the Mistag scaling 20%
  for(int fitScenario=0; fitScenario<5; fitScenario++)
    continuousPDF(6, 5, 2, 50, fitScenario);
  
  // for the Mistag scaling 20%
  for(int fitScenario=0; fitScenario<5; fitScenario++)
    continuousPDF(6, 5, 2, 20, fitScenario);
  // -------------------------------------------------
  

  // -------------------------------------------------
  // emu 2j channel
  // for the Mistag scaling 75%
  for(int fitScenario=0; fitScenario<5; fitScenario++)
    continuousPDF(6, 5, 0, 75, fitScenario);
  
  // for the Mistag scaling 20%
  for(int fitScenario=0; fitScenario<5; fitScenario++)
    continuousPDF(6, 5, 0, 50, fitScenario);
  
  // for the Mistag scaling 20%
  for(int fitScenario=0; fitScenario<5; fitScenario++)
    continuousPDF(6, 5, 0, 20, fitScenario);
  // -------------------------------------------------
   
}
