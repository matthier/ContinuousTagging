rm out_charm.root
rm out_tau.root

echo -e '\n'
echo 'Charm ttbar calibation'

export DIR=../Data/CalibrationResults/analyses/2016-20_7-13TeV/cjets/ttbarC/continuous/
./continuousTandP $DIR/CDIINPUT__85.txt \
       $DIR/CDIINPUT__77.txt \
       $DIR/CDIINPUT__70.txt \
       $DIR/CDIINPUT__60.txt

echo -e '\n'
echo 'Tau ttbar calibation'
export DIR=../Data/CalibrationResults/analyses/2016-20_7-13TeV/cjets/ttbarC/continuous/
./continuousTandP $DIR/CDIINPUT__TAU_85.txt \
       $DIR/CDIINPUT__TAU_77.txt \
       $DIR/CDIINPUT__TAU_70.txt \
       $DIR/CDIINPUT__TAU_60.txt
