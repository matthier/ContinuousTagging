/*!
   \file readfile.h
   \brief Retrieve calibration results from text files
*/

#ifndef READFILE_H
#define READFILE_H

#include <iostream>
#include <fstream>
#include <string>

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TFile.h>
#include <TString.h>

#include "operatingpoint.h"

bool verbose_read = false; /*!< Print log while sparsing the text file */

using namespace std;

vector<string> substrings(string source, string beg, string del, string end);

/*!
   \brief Retrieve informations from text files following the bTagging result conventions
   \param xfile  Text file path and name for a given WP
   \param tmp     Calibration result to fill
   \return operatingpoint object containing the cumulative calibration results for the given WP
*/
void readfile(string xfile, op& tmp){
  
  map<string, double> *map_tmp;
  map<string, double> *mapu_tmp;
  char str[256];
  
  ifstream file(xfile.c_str());
  
  double sf_val, sf_stat;
  string previous_line;
  string opLine;
  
  while(file.getline(str,256)) {
    
    string ss(str,strlen(str));
    vector<string> sub = substrings(ss,"(",",",")");
    
    if (ss.find("FixedCutBEff") != string::npos)
    opLine = ss;
    
    if (ss.find("}") != string::npos and previous_line.find("}") != string::npos)
    continue;
    
    if (ss.find("Op")!=string::npos) continue;
    
    if (ss.find("bin(")!=string::npos) {
      tmp.pt.push_back(sub[0]);
      tmp.eta.push_back(sub[1]);
      map_tmp  = new map<string, double>;
      mapu_tmp = new map<string, double>;
    }
    
    if (ss.find("central_value")!=string::npos) {
      sf_val =  atof(sub[0].c_str());
      sf_stat = atof(sub[1].c_str());
      if (ss.find("%")!=string::npos) sf_stat = sf_stat/100.*sf_val;
      tmp.sf.push_back(sf_val);
      tmp.sf_stat.push_back(sf_stat);
    }
    
    if (ss.find("extrap ratio")!=string::npos && ss.find("meta_data")!=string::npos) {
      double valx = atof(sub[1].c_str());
      tmp.extratio.push_back(valx);
    }
    
    if (ss.find("extrap factor")!=string::npos) {
      double val1 =  atof(sub[1].c_str());
      tmp.extfact.push_back(val1);
    }
    
    if ( (ss.find("N jets tagged")!=string::npos)or
    (ss.find("N_jets tagged")!=string::npos) ) {
      double val1 =  atof(sub[1].c_str());
      double val2 =  atof(sub[2].c_str());
      tmp.ntag.push_back(val1);
      tmp.sig_ntag.push_back(val2);
    }
    
    if ( (ss.find("N jets total")!=string::npos)or
    (ss.find("N_jets total")!=string::npos) ) {
      double val1 =  atof(sub[1].c_str());
      double val2 =  atof(sub[2].c_str());
      tmp.n.push_back(val1);
      tmp.sig_n.push_back(val2);
    }
    
    if ( (ss.find("effRefMC")!=string::npos)or (ss.find("EffRefMC")!=string::npos)) {
      double val1 =  atof(sub[1].c_str());
      tmp.effMC.push_back(val1);
    }
    
    if (ss.find("usys")!=string::npos) {
      string sname = sub[0];
      double tmp = atof(sub[1].c_str());
      if (ss.find("%")!=string::npos) tmp = tmp/100.*sf_val;
      mapu_tmp->insert(pair<std::string,double>(sname,tmp));
    }
    
    if (ss.find("sys")!=string::npos and ss.find("usys")==string::npos) {
      string sname = sub[0];
      if (sname == "extrapolationfromcharm") sname = "extrapolation from charm";
      double tmp = atof(sub[1].c_str());
      if (ss.find("%")!=string::npos) tmp = tmp/100.*sf_val;
      map_tmp->insert(pair<std::string,double>(sname,tmp));
    }
    
    
    if (ss.find("}")!=string::npos) {
      tmp.sf_sys.push_back(*map_tmp);
      tmp.sf_usys.push_back(*mapu_tmp);
    }
    
    previous_line = ss;
  }
  
  if((int)tmp.effMC.size() == 0)
  tmp.effMC.resize((int)tmp.n.size(),0);
}


/*!
   \brief Retrieve tokens from a source string
   \param source  Initial string
   \param beg     Symbol to start parsing
   \param end     Symbol to stop parsing
   \param del     Symbol delimiting tokens
   \return vector<string> of all retrieved tokens
*/
vector<string> substrings(string source, string beg, string del, string end)
{
  vector<string> sub;
  int b = source.find(beg.c_str());
  int e = source.find(end.c_str());
  string substring = source.substr(b+1,e-b-1);
  substring = TString(substring).ReplaceAll(" ","");

  while(substring.find(del.c_str()) != string::npos) {
    int i = substring.find(del.c_str());
    string ss = substring.substr(0, i);
    sub.push_back(ss);
    substring = substring.substr(i+1, e-i-1);
  }
  sub.push_back(substring);
  
  return sub;
}
#endif
